<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Games extends Model
{
    protected $primaryKey = 'game_id';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id', 'game_name'
    ];

    public function provider(){
        return $this->belongsTo('App\Providers', 'provider_id');
    }

    public function betwins(){
        return $this->hasMany('App\Betwins', 'game_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM games');
    }


    
}
