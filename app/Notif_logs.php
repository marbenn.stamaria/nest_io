<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Notif_logs extends Eloquent
{

	protected $connection = 'mongodb';
    protected $collection = 'notif_logs2';

    protected $fillable = [
        'recipient_id', 'message', 'read_at'
    ];    

}
