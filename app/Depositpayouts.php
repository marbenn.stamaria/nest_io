<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Depositpayouts extends Model
{
    protected $primaryKey = 'depositpayout_id';

    protected $fillable = [
        'terminal_session_id', 'cashier_id', 'depositpayout_type', 'depositpayout_amount', 'depositpayout_currency', 'depositpayout_req_datetime'
    ];

    public function terminal_session(){
        return $this->belongsTo('App\Terminal_sessions', 'terminal_session_id');
    }

    public function cashier(){
        return $this->belongsTo('App\Cashiers', 'cashier_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM depositpayouts');
    }

}
