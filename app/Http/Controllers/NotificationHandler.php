<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ModelServices\NotifLogs\NotifLogsServices;
use App\Events\NotificationTriggered;
class NotificationHandler extends Controller
{

    public function __construct()
    {
        $this->handler = new NotifLogsServices;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->handler->list();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->input('event_broadcast_id')){
            if( $this->handler->find_eventid($request->input('event_broadcast_id')) ){
                //continue the insert
                return $this->handler->insert($request->input());
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return $this->handler->show($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        //
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function user(Request $request, $id)
    {      
        return $this->handler->getAllByRecipient($id, $request->input('role'), $request->input('mode'));
    }

    //System Notification
    public function send(Request $request){
        $input = $request->input();
        $input['type'] = 'system';
        $checkparams = $this->handler->checkParameters($input, ['title', 'message', 'outlet_id', 'event']);
        if(count($checkparams) == 0){
            //Send to Individual Channel
            event(new NotificationTriggered($input));

            //Send to All Channel
            $toAll = $input;
            $toAll['outlet_id'] = 'all';
            event(new NotificationTriggered($toAll));
        }else{
            return response()->json([
                                    'response'=>false, 
                                    'message'=>'Please check the payload. Required outlet_id, title, message, event as parameters. ', 
                                    'data'=>$request->input(), 
                                    'missing'=>$checkparams
                                    ]);
        }
    }

    //Manual Notification
    public function manual(Request $request){
        $input = $request->input();
        $input['noted'] = [];
        $input['type'] = 'manual';
        $input['event'] = 'manual';

        $checkparams = $this->handler->checkParameters($input, ['title', 'message', 'outlet_id', 'event']);
        if(count($checkparams) == 0){
            //Send to Individual Channel
            event(new NotificationTriggered($input));

            //Send to All Channel
            $toAll = $input;
            $toAll['outlet_id'] = 'all';
            event(new NotificationTriggered($toAll));
        }else{
            return response()->json([
                                    'response'=>false, 
                                    'message'=>'Please check the payload. Required outlet_id, title, message, event as parameters. ', 
                                    'data'=>$request->input(), 
                                    'missing'=>$checkparams
                                    ]);
        }
    }

    public function notedManual(Request $request){
        $input = $request->input();
        $checkparams = $this->handler->checkParameters($input, ['user_id','role','outlet_id', '_id']);
        if(count($checkparams) == 0){
            $updateRow = $this->handler->notedBy($input);
        }else{
            return response()->json([
                                    'response'=>false, 
                                    'message'=>'Please check the payload. Required outlet_id, title, message, event as parameters. ', 
                                    'data'=>$request->input(), 
                                    'missing'=>$checkparams
                                    ]);
        }

        return response()->json([
            'response'=>true
            ]);
    }

    public function saveNotification(Request $request){
        $input = $request->input();
        $this->handler->saveBlob($input);
    }
    public function markAsReadNotificaiton(Request $request){
        return $this->handler->markAsRead($request->input('_id'), $request->input('user_id'), $request->input('role'));
    }
}
