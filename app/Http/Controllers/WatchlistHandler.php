<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ViewsBcfRemainingWatchlists;
use App\ViewsStatsViaDailyOutletsProviders;
use App\ViewsStatsViaDailyOutlets;
use App\Events\WatchlistUpdated;
class WatchlistHandler extends Controller
{
    public function chartData(ViewsBcfRemainingWatchlists $bcf_remaining, ViewsStatsViaDailyOutletsProviders $daily_stat_per_provider, ViewsStatsViaDailyOutlets $daily_stat_per_outlet, Request $request){
        $input = $request->input();
        $response['daily_stat_outlet']['all'] = $daily_stat_per_outlet->dataBlob();
        $response['daily_stat_outlet']['rankings'] = $daily_stat_per_outlet->rankingsBlob();
        
        $response['daily_stat_provider']['all'] = $daily_stat_per_provider->dataBlob();
        $response['daily_stat_provider']['rankings'] = $daily_stat_per_provider->rankingsBlob();

        $response['bcf_remaining'] = $bcf_remaining->dataBlob();
        event(new WatchlistUpdated($input));
        return response()->json($response);
    }

    public function getNewestInsert(ViewsBcfRemainingWatchlists $bcf_remaining, Request $request){
    return response()->json($bcf_remaining->getTransaction($request->input('id')));
    }
}
