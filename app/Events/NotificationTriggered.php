<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use App\ModelServices\NotifLogs\NotifLogsServices;

class NotificationTriggered implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $input;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($input)
    {
        //
        $this->input = $input;
        $this->handler = new NotifLogsServices;

        $hash = md5(json_encode($input));
        if($this->handler->findEvent($hash) == 0){
            if($input['outlet_id'] != 'all'){
                $this->handler->saveBlob($input, $hash);
            }
        }

    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('notification');
    }

    public function broadcastAs(){
        $req = $this->input;
        return ($req['outlet_id']=='all') ? 'notif_event='.$req['event']."_all" :  'notif_event='.$req['event']."_outlet".$req['outlet_id'];
    }
}
