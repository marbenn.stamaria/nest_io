<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Players extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'player_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_first_name', 'user_id_created','player_middle_name', 'player_last_name', 
        'player_suffix_name', 'player_given_card', 'player_account_number',
        'player_birthdate'
    ];

    public function terminal_sessions(){
        return $this->hasMany('App\Terminal_sessions', 'player_id');
    }

    public function user(){
        return $this->belongsTo('App\User', 'user_id_created');
    }

    public function betwins(){
        return $this->hasManyThrough('App\Betwins', 'App\Terminal_sessions', 'player_id', 'terminal_session_id', 'player_id', 'terminal_session_id');
    }

    public function depositpayouts(){
        return $this->hasManyThrough('App\Depositpayouts', 'App\Terminal_sessions', 'player_id', 'terminal_session_id', 'player_id', 'terminal_session_id');
    }

    public function getTerminalsPlayed(){
        return $this->hasManyThrough('App\Terminals', 'App\Terminal_sessions', 'player_id', 'terminal_id', 'player_id', 'terminal_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM players');
    }

    public function basicDepositpayouts(){
        return $this->hasManyThrough('App\Depositpayouts', 'App\Terminal_sessions', 'player_id', 'terminal_session_id', 'player_id', 'terminal_session_id')->with('cashier')->orderBy('depositpayout_req_datetime', 'desc')->limit(5);
    }

    public function basicBetwins(){
        return $this->hasManyThrough('App\Betwins', 'App\Terminal_sessions', 'player_id', 'terminal_session_id', 'player_id', 'terminal_session_id')->with('gameWithProvider')->orderBy('betwin_req_datetime', 'desc')->limit(5);
    }
}
