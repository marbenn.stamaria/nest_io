<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Operators extends Model
{
    protected $primaryKey = 'operator_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'operator_name'
    ];

    public function outlets(){
        return $this->hasMany('App\Outlets', 'operator_id');
    }

    public function cashiers(){
        return $this->hasMany('App\Cashiers', 'operator_id');
    }

    public function operator_user_accounts(){
        return $this->hasManyThrough('App\Users', 'App\Operator_users','operator_id', 'id', 'operator_id', 'user_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM operators');
    }
}
