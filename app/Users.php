<?php

namespace App;
use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
// use Spatie\Permission\Traits\HasRoles;

class Users extends Authenticatable
{
    use Notifiable;
    // use HasRoles;

    protected $guard_name = 'web';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 'firstname', 'lastname', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function players(){
        return $this->hasMany('App\Players', 'user_id_created');
    }

    public function cashier(){
        return $this->hasOne('App\Cashiers', 'user_id_using');
    }

    public function cashiersCreated(){
        return $this->hasMany('App\Cashiers', 'user_id_created');
    }

    public function operatorHandle(){
        return $this->hasOne('App\Operator_users', 'user_id');
    }

    public function getConcernedOutlets(){
        // dd($this->operatorHandle()->first()->operator()->first()->outlets );
        // dd($this->cashier->outlet->outlet_id );
        $concerned_outlets = [];
        $role = Auth::user()->getRoleNames()->first();
        switch ($role) {
            case 'cashiers':
                $concerned_outlets[] = $this->cashier->outlet->outlet_id;
                break;
            case 'operators':
                foreach ($this->operatorHandle()->first()->operator()->first()->outlets as $key => $outlet) {
                    $concerned_outlets[] = $outlet->outlet_id;
                }
                break;
            
            default:
                $concerned_outlets = [];
                break;
        }
        return $concerned_outlets;
    }
}
