<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class CashierOutletAllocations extends Model
{

    use SoftDeletes;

    protected $primaryKey = 'cashier_outlet_allocation_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'outlet_id', 'cashier_id', 'assigner_id'
    ];

    public function outlet(){
        return $this->belongsTo('App\Outlets', 'outlet_id');
    }

    public function cashier(){
        return $this->belongsTo('App\Cashiers', 'cashier_id');
    }

    public function Assigner(){
        return $this->belongsTo('App\Users', 'assigner_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM outlets');
    }
}
