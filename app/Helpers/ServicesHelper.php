<?php

namespace App\Helpers;
use Auth;
use App\Games;
use App\Branches;
use App\Helpers\FormFilterHandler;

class ServicesHelper{
    public function data4Datatables($availableColumns, $allowedColumns = null, $unset = true){

        $returnArray = [];
        $tempArrays = [];

        if(!empty($allowedColumns)){
            $allowedColumns = $allowedColumns;
            $filteredColumns = array_filter($availableColumns, function ($key) use ($allowedColumns){
                return in_array($key, $allowedColumns);
            }, ARRAY_FILTER_USE_KEY);
            $tempArrays = $filteredColumns;
        }else{
            $tempArrays = $availableColumns;
        }

        foreach($tempArrays as $tempArray => $columnValues){
            if($unset){
                unset($columnValues['columnName']);
            }
            array_push($returnArray, $columnValues);
        }

        return $returnArray;
       
    }

    public function generateColumnAliases($allColumns){
        $availableColumns = [];
        foreach($allColumns as $tableName => $Columns){
            foreach($Columns as $Column){
                if($Column->Field != 'created_at' && $Column->Field != 'updated_at'){
                    $dataToInsert = ['columnName' => $tableName . '.'. $Column->Field, 'data' => md5($tableName . '.'. $Column->Field), 'title' => json_decode($Column->Comment, true)['title']];
                    if(!in_array($dataToInsert, $availableColumns, true)){
                        if( (! strpos($Column->Field, '_id') ) && (! strpos($Column->Field, '_raw')) ){
                            $availableColumns[$dataToInsert['data']] = $dataToInsert;
                        }
                    }
                }
            }
        }

        return $availableColumns;
    }

    public function getGameName($id){
        return Games::findOrFail($id);
    }

    public function getBranchName($id){
        return Branches::findOrFail($id);
    }

    public function getColumnNames($request_selected_columns = null, $columns_ = null, $force = false){
        $all_columns = $columns_;
        $selectColumns = [];
        if($columns_ !== null){
            if(!empty($request_selected_columns)){
            $selected_columns = explode(",",urldecode($request_selected_columns));
                foreach($selected_columns as $key => $value){
                    if($force){
                        if(in_array($value, $all_columns)) {
                            array_push($selectColumns, $value);
                        }
                    }else{
                        if(array_search($value, array_column($all_columns, 'Field')) !== False) {
                            array_push($selectColumns, $value);
                        }
                    }
                }
            }else{
                foreach($all_columns as $key=>$column){
                    if($force){
                        array_push($selectColumns, $column);
                    }else{
                        array_push($selectColumns, $column->Field);
                    }
                }
            }
        }
        return $selectColumns;
    }

    public function generateFormFilter($request = null, $config = null){
        $form = new FormFilterHandler;
        $form->where = $request;
        $form->filters = $config['filters'];
        $form->action = $config['form_url'];
        $form->method = $config['form_method'];
        $form->selected_columns = $config['selected_columns'];
        $form->columns = $config['columns'];
        return $form->filterBuilder();
    }

    public function whereInSubQuer($values = [], $table = '', $column = ''){
        $quer_string = 'select '.$column.' from '.$table.' where (';
        if($table != '' && count($values) > 0){
            foreach($values as $key=>$value){
                if($key == 0){
                    $quer_string .= $column.' = '.$value;
                }else{
                    $quer_string .= ' or '.$column.' = '.$value;
                }
            }
        }
        return $quer_string.')';
    }

    public static function getChannelName($role, $event = ''){
        $event_names = [];
        $events = array_keys(config('site.notif_events'));
        switch ($role) {
            case 'cashiers': 
                switch ($event) {
                    default:
                        $event_names[] = 'notif'.config('site.sitecode').'-'.$event.'-outlet'.Auth::user()->cashier()->get()->first()->outlet()->get()->first()->outlet_id;
                        break;
                }
                break;
            case 'operators':    
                switch ($event) {
                    case 'low-bcf':
                        // $event_names[] = 'notif'.config('site.sitecode').'-'.Auth::user()->operator()->get()->first()->operator_id;

                        //GET ALL OUTLETS
                        //PUSH TO event_names THE OUTLET IDs with format notif[sitecode]-[event]-outlet[outlet_id]
                        break;
                    
                    default:
                        $event_names[] = 'notif'.config('site.sitecode').'-'.$event;
                        break;
                }
                break;
        
            default:
                $event_names[] = 'notif'.config('site.sitecode').'-'.$event;
                break;
        }
        return $event_names;
    }
}