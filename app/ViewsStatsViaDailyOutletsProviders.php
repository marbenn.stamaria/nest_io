<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ViewsStatsViaDailyOutletsProviders extends Model
{
    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM views_stats_via_daily_outlets_providers');
    }
    public static function dataBlob($outlet_id = null){
        if($outlet_id != null){
            $data = self::select()->where('outlet_id', $outlet_id)->get()->toArray();
        }else{
            $data = self::select()->get()->toArray();
        }
        return $data;
    }
    public static function rankingsBlob(){
        $data = [];
        $data['top_earnings'] = self::select()->orderBy('Cash In Hand', 'desc')->limit(10)->get()->toArray();
        return $data;
    }

}
