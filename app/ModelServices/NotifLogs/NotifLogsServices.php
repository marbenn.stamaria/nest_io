<?php

namespace App\ModelServices\NotifLogs;

use App\Helpers\ServicesHelper;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use App\Notif_logs;
use App\Outlets;
use App\Operator_users;
use App\Operators;
use App\Users;

class NotifLogsServices
{
    
    private $data = [];

    protected $notif_Logs;
    
    public function __get($propertyName){
        if(array_key_exists($propertyName, $this->data)){
            return $this->data[$propertyName];
        }    
    } 
    
    public function __set($propertyName, $propertyValue){
        $this->data[$propertyName] = $propertyValue;
    } 

    public function __construct(Notif_logs $notif_Logs = null){
        $this->notif_Logs = $notif_Logs;
        $this->ServicesHelper = new ServicesHelper;
    }

    public function list(){

        $notif_log = Notif_logs::select()->get();
        return $notif_log;

    }

    public function findEvent($_id){
        $notif_log = Notif_logs::select()->where('_id', $_id)->get()->count();
        return $notif_log;
    }

    public function find_recipient($recipient){
        $notif_log = Notif_logs::select()->where('recipient_id', $recipient)->get();
        return $notif_log;

    }

    public function find_eventid($event){
        return empty(Notif_logs::select()->where('event_id', $event)->get()->toArray());

    }


    public function getAllByRecipient( $recipientId, $role, $mode = null){
        switch ($role) {
            case 'cashiers':
            case 'operators':
                $notif_log['total_msg'] = Notif_logs::where('recipients.'.$recipientId, 'exists', true)->select('_id')->get()->count();
                $notif_log['total_unread'] = Notif_logs::where('recipients.'.$recipientId, 'exists', true)->where('recipients.'.$recipientId.'.read_at', '')->get()->count();
                if($mode != null){
                    $notif_log['result'] = Notif_logs::where('recipients.'.$recipientId, 'exists', true)->orderBy('created_at', 'desc')->get();
                }else{
                    $notif_log['result'] = Notif_logs::where('recipients.'.$recipientId, 'exists', true)->orderBy('created_at', 'desc')->limit(5)->get();
                }
                break;
                
                default:
                $notif_log['total_msg'] = Notif_logs::where('recipients.all', 'exists', true)->select('_id')->get()->count();
                $notif_log['total_unread'] = Notif_logs::where('recipients.all', 'exists', true)->where('recipients.all.'.$recipientId, 'exists', false)->get()->count();
                
                if($mode != null){
                    $notif_log['result'] = Notif_logs::where('recipients.all', 'exists', true)->orderBy('created_at', 'desc')->get();
                }else{
                    $notif_log['result'] = Notif_logs::where('recipients.all', 'exists', true)->orderBy('created_at', 'desc')->limit(5)->get();
                }
                break;
        }


        return $notif_log;

    }

    public function insert( $arr ){
        $notif_log = new Notif_logs;
        $notif_log->event_id = $arr['event_broadcast_id'];
        $notif_log->event = $arr['event'];
        $notif_log->type = $arr['type'];
        $notif_log->recipient_id = $arr['recipient_id'];
        $notif_log->title = $arr['title'];
        $notif_log->message = $arr['message'];
        $notif_log->read_at = '';

        if($arr['type'] == 'manual'){
            $notif_log->noted = $arr['noted'];
        }

        $notif_log->save();

        return $notif_log;

    }

    public function saveBlob( $input, $hash ){

        if(isset($input['outlet_id'])){
            $recipients = [];
            if($input['outlet_id'] != 'all'){
                $outlet = Outlets::select()->where('outlet_id', $input['outlet_id'])->first();
                $cashiers = $outlet->cashiers;
                foreach ($cashiers as $cashier_key => $cashier) {
                    $recipients[$cashier->userAccount->id]['id'] = $cashier->userAccount->id;
                    $recipients[$cashier->userAccount->id]['role'] = 'cashier';
                    $recipients[$cashier->userAccount->id]['read_at'] = '';
                }
                $operator_id = $outlet->operator()->first()->operator_id;
                $operators = Operators::where('operator_id', $operator_id)->first()->operator_user_accounts;
                foreach ($operators as $operator_key => $operator) {
                    $recipients[$operator->id]['id'] = $operator->id;
                    $recipients[$operator->id]['role'] = 'operator';
                    $recipients[$operator->id]['read_at'] = '';
                }
            }

            //Include generanal channel ALL for cs and finops
            $recipients['all'] = [];

            $input['recipients'] = $recipients;


            if ($this->find_eventid($hash)) {
                $notif_log = new Notif_logs;
                $notif_log->event_id = $hash;
                $notif_log->event = $input['event'];
                $notif_log->type = $input['type'];
                $notif_log->recipients = $input['recipients'];
                $notif_log->title = $input['title'];
                $notif_log->message = $input['message'];
                if($input['type'] == 'manual'){
                    $notif_log->noted = $input['noted'];
                    if(!empty($input['service_id'])){
                        $notif_log->service_id = $input['service_id'];

                        if(!empty($input['service_request_url'])){
                            $notif_log->service_request_url = $input['service_request_url'];
                        }
    
                        if(!empty($input['ticket'])){
                            $notif_log->ticket = $input['ticket'];
                        }
                    }

                }
                $notif_log->save();
                return $notif_log;
            }else{
                return false;
            }

        }
    }

    public function show( $notifId ){

        $notif_log = Notif_logs::find( $notifId );
        return $notif_log;

    }

    public function markAsRead( $notifId, $user_id, $role ){

        $notif_log = Notif_logs::find( $notifId );
        if($notif_log->{'recipients.'.$user_id.'.read_at'} == ''){
            if($role == 'cashiers' || $role == 'operators' ){
                $notif_log->{'recipients.'.$user_id.'.read_at'} = date("Y-m-d h:i:s");
            }else{
                $seen = $notif_log->recipients['all'];
                if( !isset($seen[$user_id]) ){
                    $seen[$user_id] = [
                                        'read_at'=>date("Y-m-d h:i:s"),
                                        'role'=>$role,
                                        'id'=>$user_id
                                      ];
                }
                $notif_log->{'recipients.all'} = $seen;
                // $notif_log->{'recipients.all'} = date("Y-m-d h:i:s");
            }
        }
        $notif_log->save();
        return $notif_log;
    }

    public function notedBy( $obj ){
        $notif_log = Notif_logs::find( $obj['_id'] );
        
        $noted = $notif_log->noted;
        $noted[$obj['user_id']] =  [
            'user_id'=>$obj['user_id'], 
            'outlet_id'=>$obj['outlet_id'], 
            'role'=>$obj['role'], 
            'read_at'=> date("Y-m-d h:i:s") 
        ];
        $notif_log->noted = $noted;
        $notif_log->save();
    }

    public function checkParameters($input, $params = null){
        $missing = [];
        for ($i=0; $i < count($params); $i++) { 
            if(!isset($input[$params[$i]])){
                $missing[] = $params[$i];
            }
        }
        return $missing;
    }

}