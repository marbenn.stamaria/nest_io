<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Bcf_transactions extends Model
{
    protected $primaryKey = 'bcf_transaction_id';

    protected $table = 'bcf_transactions';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id', 'outlet_id', 'depositpayout_id', 'bcf_addition_id', 'bcf_transaction_amount', 'bcf_transaction_remaining', 'bcf_transaction_last_total', 'bcf_transaction_currency'
    ];
    


    public function provider(){
        return $this->belongsTo('App\Providers', 'provider_id');
    }

    public function outlet(){
        return $this->belongsTo('App\Outlets', 'outlet_id');
    }

    public function depositpayout(){
        return $this->belongsTo('App\Depositpayouts', 'depositpayout_id');
    }

    public function bcfadditionInformation(){
        return $this->belongsTo('App\Bcf_additions', 'bcf_addition_id');
    }



    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM bcf_transactions');
    }
}
