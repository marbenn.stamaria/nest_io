<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ViewsStatsViaDailyOutlets extends Model
{
    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM views_stats_via_daily_outlets');
    }
    public static function dataBlob(){
        $data = self::select()->get()->toArray();
        return $data;
    }
    public static function rankingsBlob(){
        $data = [];
        $data['top_unique_players'] = self::select()->orderBy('Unique Players Played', 'desc')->limit(10)->get()->toArray();
        $data['top_unique_sessions'] = self::select()->orderBy('Sessions Played', 'desc')->limit(10)->get()->toArray();
        return $data;
    }
}
