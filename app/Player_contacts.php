<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Player_contacts extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'player_contact_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'player_id', 'player_contact_origin', 'player_contact_value'
    ];

    public function player(){
        return $this->belongsTo('App\Players', 'player_id');
    }


    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM terminals');
    }
}
