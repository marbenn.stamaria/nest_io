<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Betwins extends Model
{
    protected $primaryKey = 'betwin_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'terminal_session', 'game_id', 'betwin_type', 'betwin_amount', 'betwin_currency', 'betwin_req_datetime', 'betwin_raw'
    ];
    


    public function terminal_session(){
        return $this->belongsTo('App\Terminal_sessions', 'terminal_session_id');
    }

    public function game(){
        return $this->belongsTo('App\Games', 'game_id');
    }

    public function gameWithProvider(){
        return $this->belongsTo('App\Games', 'game_id')->with('provider');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM betwins');
    }

    
}
