<?php

namespace App;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Database\Eloquent\Model;

class Operator_users extends Model
{
    use SoftDeletes;

    
    protected $fillable = [
        'operator_id', 'user_id','assigner_id'
    ];

    public function operator(){
        return $this->belongsTo('App\Operators', 'operator_id');
    }

    public function userAccount(){
        return $this->belongsTo('App\Users', 'user_id');
    }

    public function Assigner(){
        return $this->belongsTo('App\Users', 'assigner_id');
    }


}
