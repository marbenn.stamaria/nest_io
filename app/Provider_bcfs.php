<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Provider_bcfs extends Model
{

    use SoftDeletes;

    protected $primaryKey = 'provider_bcf_id';
    protected $table = 'provider_bcfs';
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id', 'outlet_id', 'provider_bcf_amount', 'provider_bcf_currency'
    ];

    public function provider(){
        return $this->belongsTo('App\Providers', 'provider_id');
    }
    
    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM provider_bcfs');
    }
}
