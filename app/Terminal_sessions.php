<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Terminal_sessions extends Model
{

    use SoftDeletes;

    protected $primaryKey = 'terminal_session_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'terminal_session_code', 'terminal_session_deposit_amount', 'terminal_session_withdraw_amount', 'terminal_session_currency'
    ];

    public function player(){
        return $this->belongsTo('App\Players', 'player_id');
    }

    public function terminal(){
        return $this->belongsTo('App\Terminals', 'terminal_id');
    }

    public function provider(){
        return $this->belongsTo('App\Providers', 'provider_id');
    }

    public function cashier(){
        return $this->belongsTo('App\Cashiers', 'cashier_id');
    }
    
    public function betwins(){
        return $this->hasMany('App\Betwins', 'terminal_session_id');
    }

    public function depositpayouts(){
        return $this->hasMany('App\Depositpayouts', 'terminal_session_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM terminal_sessions');
    }
}
