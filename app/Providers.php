<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Providers extends Model
{
    protected $primaryKey = 'provider_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_name'
    ];

    public function games(){
        return $this->hasMany('App\Games', 'provider_id');
    }

    public function terminal_sessions(){
        return $this->hasMany('App\Terminal_sessions', 'provider_id');
    }

    public function providers_bcfs(){
        return $this->hasMany('App\Provider_bcfs', 'provider_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM providers');
    }

}
