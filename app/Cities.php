<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cities extends Model
{

    protected $primaryKey = 'city_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'region_id', 'city_name' , 'psgc_code', 'province_id', 'city_code'
    ];


    public function region(){
        return $this->belongsTo('App\Regions', 'region_id');
    }

    public function outlets(){
        return $this->hasMany('App\Outlets', 'city_id');
    }

}
