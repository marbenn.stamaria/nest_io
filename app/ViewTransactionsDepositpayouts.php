<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class ViewTransactionsDepositpayouts extends Model

{
    public static function getDistincts($entity){
        $entity_ = explode(' ', $entity);
        return self::select($entity_[0].' ID as id', $entity)->distinct()->get();
    }

    public static function getDetails($id, $prefix){
        return self::select($prefix.' ID as id', $prefix.' Name as name')->where('id', $id)->get();
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM view_transactions_depositpayouts');
    }
}
