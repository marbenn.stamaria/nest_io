<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Regions extends Model
{

    protected $primaryKey = 'region_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'region_name'
    ];

    public function cities(){
        return $this->hasMany('App\Cities', 'region_id');
    }
}
