<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Bcf_additions extends Model
{
    protected $primaryKey = 'bcf_addition_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'provider_id', 'outlet_id', 'added_by_user_id', 'bcf_addition_amount', 'bcf_addition_currency'
    ];
    


    public function provider(){
        return $this->belongsTo('App\Providers', 'provider_id');
    }

    public function outlet(){
        return $this->belongsTo('App\Outlets', 'outlet_id');
    }

    public function AddedBy(){
        return $this->belongsTo('App\Users', 'added_by_user_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM bcf_additions');
    }
}
