<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provinces extends Model
{
    protected $primaryKey = 'province_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'region_id', 'province_name' , 'psgc_code', 'province_code'
    ];


    public function region(){
        return $this->belongsTo('App\Regions', 'region_id');
    }

    public function cities(){
        return $this->hasMany('App\Cities', 'province_id');
    }
}
