<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;


class Terminals extends Model
{
    use SoftDeletes;
    
    protected $primaryKey = 'terminal_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'terminal_name'
    ];

    public function outlet(){
        return $this->belongsTo('App\Outlets', 'outlet_id');
    }

    public function city(){
        return $this->belongsTo('App\Cities', 'city_id');
    }
    
    public function terminal_sessions(){
        return $this->hasMany('App\Terminal_sessions', 'terminal_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM terminals');
    }
}
