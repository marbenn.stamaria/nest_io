<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Bcf_transactions;
class ViewsBcfRemainingWatchlists extends Model
{
    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM view_transactions_betwins');
    }
    public static function dataBlob(){

        $data['recent'] = Bcf_transactions::select()->orderBy('created_at', 'desc')->limit(5)->get()->toArray();

        $data['results'] = self::select()->orderBy('bcf_transaction_remaining', 'asc')->limit(15)->get()->toArray();
        
        $data['stats']['top'] = self::select()->orderBy('bcf_transaction_remaining', 'desc')->limit(15)->get()->toArray();
        $data['stats']['least'] = self::select()->orderBy('bcf_transaction_remaining', 'asc')->limit(15)->get()->toArray();
        
        return $data;
    }
    public static function getTransaction($id){
        return Bcf_transactions::select()->orderBy('created_at', 'desc')->where('bcf_transaction_id', $id)->limit(1)->get()->toArray();
    }
}
