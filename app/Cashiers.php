<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cashiers extends Model
{
    use SoftDeletes;

    protected $primaryKey = 'cashier_id';

    protected $fillable = [
        'user_id_created', 'user_id_using','cashier_first_name','cashier_middle_name', 'cashier_last_name', 
        'cashier_suffix_name', 'cashier_given_card', 'cashier_gel_number',
        'cashier_birthdate', 'operator_id'
    ];

    public function userCreator(){
        return $this->belongsTo('App\Users', 'user_id_created');
    }

    public function userAccount(){
        return $this->belongsTo('App\Users', 'user_id_using');
    }

    public function outlet(){
        return $this->belongsTo('App\CashierOutletAllocations', 'cashier_id');
    }

    public function operator(){
        return $this->belongsTo('App\Operators', 'operator_id');
    }

    public function terminal_sessions(){
        return $this->hasMany('App\Terminal_sessions', 'cashier_id');
    }

    public function depositpayouts(){
        return $this->hasMany('App\Depositpayouts', 'cashier_id');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM cashiers');
    }
}
