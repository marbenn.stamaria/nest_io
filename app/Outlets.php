<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Outlets extends Model
{
    protected $primaryKey = 'outlet_id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'city_id', 'outlet_name', 'outlet_address', 'outlet_contact1', 'outlet_contact2', 'outlet_IP'
    ];

    public function operator(){
        return $this->belongsTo('App\Operators', 'operator_id');
    }

    
    public function city(){
        return $this->belongsTo('App\Cities', 'city_id');
    }
    
    public function terminals(){
        return $this->hasMany('App\Terminals', 'outlet_id');
    }

    public function cashiers(){
        return $this->hasManyThrough('App\Cashiers', 'App\CashierOutletAllocations', 'outlet_id', 'cashier_id', 'outlet_id', 'cashier_id')->with('userAccount');
    }

    public static function getColumns(){
        return DB::select('SHOW FULL COLUMNS FROM outlets');
    }
}
