<?php

use Illuminate\Http\Request;
use App\Helpers\NotificationHelper;
use App\ViewsBcfRemainingWatchlists;
use App\ViewsStatsViaDailyOutletsProviders;
use App\ViewsStatsViaDailyOutlets;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('notifications', 'NotificationHandler');
Route::resource('make/notification', 'NotificationMaker');
Route::get('notifications/user/{id}', 'NotificationHandler@user');


Route::post('retrieve/notifications', function (Request $request) {
    $user = $request->input();
    return view('notifications.list', compact('user'));
});

Route::get('get/watchlist', function(ViewsBcfRemainingWatchlists $bcf_remaining, ViewsStatsViaDailyOutletsProviders $daily_stat_per_provider, ViewsStatsViaDailyOutlets $daily_stat_per_outlet, Request $request){
    $input = $request->input();
    $response['daily_stat_outlet']['all'] = $daily_stat_per_outlet->dataBlob();
    $response['daily_stat_outlet']['rankings'] = $daily_stat_per_outlet->rankingsBlob();
    
    $response['daily_stat_provider']['all'] = $daily_stat_per_provider->dataBlob();
    $response['daily_stat_provider']['rankings'] = $daily_stat_per_provider->rankingsBlob();

    $response['bcf_remaining'] = $bcf_remaining->dataBlob();
    return response()->json($response);
});

Route::post('get/watchlist/outlet', function(ViewsStatsViaDailyOutletsProviders $daily_stat_per_provider, Request $request){
    return response()->json($daily_stat_per_provider->dataBlob($request->input('outlet_id')));
});

Route::get('retrieve/samplegraph', function (Request $request) {
    $user = $request->input();
    return view('graphs.sample', compact('user'));
});

Route::post('get/terminals', function(Request $request){
        $terminals = App\Terminals::where('outlet_id', $request->input('outlet_id'))->orderBy('terminal_name')->get();
        return response()->json($terminals);
});
Route::get('retrieve/terminals', function (Request $request) {
    $user = $request->input();
    return view('terminals.list', compact('user'));
});

Route::get('watchlist/get', 'WatchlistHandler@chartData');
Route::post('watchlist/get', 'WatchlistHandler@chartData');


Route::post('watchlist/transaction-ticker', 'WatchlistHandler@getNewestInsert');

Route::post('send/notifications', 'NotificationHandler@send');
Route::post('send/notifications/manual', 'NotificationHandler@manual');
Route::post('send/notifications/manual/noted', 'NotificationHandler@notedManual');
Route::post('notifications/save', 'NotificationHandler@saveNotification');
Route::post('notifications/markasread', 'NotificationHandler@markAsReadNotificaiton');


Route::get('demo/initiate/deposit', function(Request $request){
    $input = $request->input();
    $input['type'] = 'deposit';

    event(new App\Events\DemoEvent($input));
});
Route::get('demo/initiate/payout', function(Request $request){
    $input = $request->input();
    $input['type'] = 'payout';

    event(new App\Events\DemoEvent($input));
});