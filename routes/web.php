<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('test', 'Marbenn');

Route::get('/basic', function(){
    return view('io');
});

Route::get('/trigger', function(){
    return view('trigger');
});

Route::post('/trigger', function(){
    $text = request()->input('text');
    event(new App\Events\NotificationTriggered($text));
    return request()->input();
});