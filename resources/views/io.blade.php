<!DOCTYPE html>
<head>
  <title>Pusher Test</title>
  <script src="http://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA=" crossorigin="anonymous"></script>
  <script src="https://js.pusher.com/4.4/pusher.min.js"></script>
  <script>

    // Enable pusher logging - don't include this in production
    Pusher.logToConsole = true;

    var pusher = new Pusher('490d18d82d3c4adf483c', {
      cluster: 'ap1',
      forceTLS: true
    });

    var channel = pusher.subscribe('notification');
    channel.bind('notification-triggered', function(data) {
      alert(JSON.stringify(data));
    });
  </script>
</head>
<body>
  <h1>Pusher Test</h1>
  <p>
    Try publishing an event to channel <code>my-channel</code>
    with event name <code>my-event</code>.
  </p>
  
  <script src="{{asset('js/app.js')}}"></script>
</body>