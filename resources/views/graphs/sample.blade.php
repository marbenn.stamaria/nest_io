<?php header('Access-Control-Allow-Origin: *'); ?>
<meta name="csrf-token" content="{{ csrf_token() }}">

<div id="graph">
    <graphs></graphs>
</div>

<script src="{{ asset('js/app.js') }}"></script>