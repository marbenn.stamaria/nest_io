<?php header('Access-Control-Allow-Origin: *'); ?>
<meta name="csrf-token" content="{{ csrf_token() }}">

<div id="terminal">
    <terminals></terminals>
</div>

<script src="{{ asset('js/app.js') }}"></script>