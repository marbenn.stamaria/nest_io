<?php header('Access-Control-Allow-Origin: *'); ?>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src='https://cdn.jsdelivr.net/npm/vue2-timeago@1.1.3/dist/vue2-timeago.min.js'></script>
<link src="https://cdn.jsdelivr.net/npm/vue2-timeago@1.1.3/dist/vue2-timeago.css" />
<div id="notification">
    <notifications user-id="584" user-role="csreps" v-bind:user-outlets="[]"></notifications>
</div>

  <script src="{{ asset('js/app.js') }}"></script>