
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
// import BootstrapVue from 'bootstrap-vue'
// import 'bootstrap/dist/css/bootstrap.css'
// import 'bootstrap-vue/dist/bootstrap-vue.css'


window.Vue = Vue;

// window.Vue = window.Vue.use(BootstrapVue);
if(document.getElementById('notification') != null){
    Vue.component('notifications', require('./components/NotificationList.vue').default);
    const app = new Vue({
        el: '#notification'
    });
}



if(document.getElementById('graph') != null){
    Vue.component('graphs', require('./components/RemainingBcfWatchlist.vue').default);
    const app = new Vue({
        el: '#graph'
    });
}


if(document.getElementById('terminal') != null){
    Vue.component('terminals', require('./components/TerminalList.vue').default);
    const app = new Vue({
        el: '#terminal'
    });
}
